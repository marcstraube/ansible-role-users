users
=========

Manage users and groups

Example Playbook
----------------

    - hosts: all
      vars:
        password_salt: mypasswordsalt
        groups_list:
	  - name: mygroup
	    gid: 2000
	  - name: grouptoremove
	    state: absent
	users_list:
          - username: myuser
            uid: 1500
            group: myusergroup
            gid: 1500
            groups:
              - users
              - wheel
            shell: /bin/bash
	    password: mypassword
	  - username: usertoremove
	    state: absent
      roles:
        - { role: marcstraube.users }

License
-------

GPLv3
